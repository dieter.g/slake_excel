VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "slakeSheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit

Private Sub Worksheet_SelectionChange(ByVal Target As Range)
  Dim selectedCell As Range, debugTest As Long
  
  If Not GameRunning Then Exit Sub
  If Target.Cells.Count > 1 Then Exit Sub
  If Target.Address = slakeSheet.Range("DirectCentre").Address Then Exit Sub
  
  Set selectedCell = Intersect(Target, slakeSheet.Range("Directions"))
  If Not selectedCell Is Nothing Then
    Select Case selectedCell.Name.Name
      Case "D_UP"
        If Right(MoveBuffer, 1) <> D_DOWN _
        And Right(MoveBuffer, 1) <> D_UP Then MoveBuffer = MoveBuffer & D_UP
      Case "D_DOWN"
        If Right(MoveBuffer, 1) <> D_UP _
        And Right(MoveBuffer, 1) <> D_DOWN Then MoveBuffer = MoveBuffer & D_DOWN
      Case "D_LEFT"
        If Right(MoveBuffer, 1) <> D_RIGHT _
        And Right(MoveBuffer, 1) <> D_LEFT Then MoveBuffer = MoveBuffer & D_LEFT
      Case "D_RIGHT"
        If Right(MoveBuffer, 1) <> D_LEFT _
        And Right(MoveBuffer, 1) <> D_RIGHT Then MoveBuffer = MoveBuffer & D_RIGHT
    End Select
    slakeSheet.Range("DirectCentre").Select
  End If
End Sub
