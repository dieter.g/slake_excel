Attribute VB_Name = "Main"
Option Explicit

Public GameRunning As Boolean
Public CurrentDirection As Long
Public MoveBuffer As String

Public Const D_UP = 1
Public Const D_DOWN = 2
Public Const D_LEFT = 3
Public Const D_RIGHT = 4
Private Const Food = 0

Private Board As Range
Private High As Range
Private Score As Range
Private Speed As Shape
Private Gauge As Shape
Private GaugeMax As Double
Private FoodLocation As Range
Private GameOver As Boolean
Private Slake() As String
Private HasFood As Boolean
Private Level As Long
Private NewLocation As Range
Private GameSpeed As Double
Private StartSpeed As Double
Private MaxSpeed As Double
Private Accelerate As Double
Private Overdrive As Double

Public Sub Game()
  Dim frameUpdate As Single, moveCompleted As Boolean
  
  ReDim Slake(0 To 0)
  Set Board = slakeSheet.Range("Board")
  Set High = slakeSheet.Range("High")
  Set Score = slakeSheet.Range("Score")
  Set Speed = slakeSheet.Shapes("Speed")
  Set Gauge = slakeSheet.Shapes("Gauge")
  Gauge.Width = 0
  GaugeMax = 96
  
  slakeSheet.Range("DirectCentre").Select
  Board.ClearContents
  Score = 0
  Speed.TextFrame2.TextRange.Text = "SLOW"
  
  Level = 1
  MaxSpeed = 0.1
  StartSpeed = 0.3
  Accelerate = 0.001
  Overdrive = 0.0005
  
  PlaceSnake
  DisplayMessage "LEVEL 1", True, 3

  GameSpeed = StartSpeed
  frameUpdate = Timer + GameSpeed
  GameRunning = True
  
  While Not GameOver
    If moveCompleted Then
      ChangeDirection
    End If
    moveCompleted = False
    If Timer >= frameUpdate Then
      frameUpdate = Timer + GameSpeed
      MoveSlake
      moveCompleted = True
      PlaceFood
    End If
    DoEvents
  Wend

  With slakeSheet
    If Score < High Then
      DisplayMessage "GAME OVER", False
    Else
      High = Score
      DisplayMessage "GAME OVER" & vbCrLf & "GG! HIGH SCORE!", False
    End If
  End With
  End
End Sub

Private Sub ChangeDirection()
  Dim oldLocation As Range, previousDirection As Long

  previousDirection = CurrentDirection
  If Len(MoveBuffer) > 0 Then
    CurrentDirection = Left(MoveBuffer, 1)
    MoveBuffer = Mid(MoveBuffer, 2)
  End If
  
  'Safeguard against traveling in the opposite direction
  If previousDirection = D_UP And CurrentDirection = D_DOWN Then
    CurrentDirection = D_UP
  ElseIf previousDirection = D_DOWN And CurrentDirection = D_UP Then
    CurrentDirection = D_DOWN
  ElseIf previousDirection = D_LEFT And CurrentDirection = D_RIGHT Then
    CurrentDirection = D_LEFT
  ElseIf previousDirection = D_RIGHT And CurrentDirection = D_LEFT Then
    CurrentDirection = D_RIGHT
  End If

  Set oldLocation = slakeSheet.Range(Slake(0))
  Select Case CurrentDirection
    Case D_UP
      Set NewLocation = oldLocation.Offset(-1)
    Case D_DOWN
      Set NewLocation = oldLocation.Offset(1)
    Case D_LEFT
      Set NewLocation = oldLocation.Offset(0, -1)
    Case D_RIGHT
      Set NewLocation = oldLocation.Offset(0, 1)
  End Select
End Sub

Public Sub MoveSlake()
  Dim oldSlake As String, newSlake As String
  
  If NewLocation Is Nothing Then ChangeDirection
  
  If NewLocation.Column < Board.Column Then
    Set NewLocation = NewLocation.Offset(0, Board.Columns.Count)
  ElseIf NewLocation.Column > Board.Column + (Board.Columns.Count - 1) Then
    Set NewLocation = NewLocation.Offset(0, -Board.Columns.Count)
  End If
  
  If NewLocation.Row < Board.Row Then
    Set NewLocation = NewLocation.Offset(Board.Rows.Count, 0)
  ElseIf NewLocation.Row > Board.Row + (Board.Rows.Count - 1) Then
    Set NewLocation = NewLocation.Offset(-Board.Rows.Count, 0)
  End If

  oldSlake = Join(Slake, ",")
  If IsEmpty(NewLocation) Then
    If UBound(Slake) > 0 Then
      newSlake = NewLocation.Address & "," & Left(oldSlake, InStrRev(oldSlake, ",") - 1)
    Else
      newSlake = NewLocation.Address
    End If
    NewLocation = 1
    slakeSheet.Range(Slake(UBound(Slake))).ClearContents
    Slake = Split(newSlake, ",")
  ElseIf NewLocation = Food Then
    NewLocation = 1
    newSlake = NewLocation.Address & "," & oldSlake
    Slake = Split(newSlake, ",")
    HasFood = False
    UpdateScore
  Else
    slakeSheet.Range(Slake(0)) = 2
    GameOver = True
  End If
  DoEvents
  
  Set NewLocation = Nothing
End Sub

Private Sub PlaceSnake()
  Dim randRow As Long, randCol As Long, goodPosition As Boolean, slakeLocation As Range
  
  ReDim Slake(0 To 0)
  
  While Not goodPosition
    Randomize
    randRow = WorksheetFunction.RandBetween(Board.Row, Board.Row + (Board.Rows.Count - 1))
    randCol = WorksheetFunction.RandBetween(Board.Column, Board.Column + (Board.Columns.Count - 1))
    
    Set slakeLocation = slakeSheet.Cells(randRow, randCol)
    If IsEmpty(slakeLocation) Then goodPosition = True
    DoEvents
  Wend
  
  Slake(0) = slakeLocation.Address
  slakeSheet.Range(Slake(0)) = 1
  CurrentDirection = D_LEFT
End Sub

Private Sub PlaceFood()
  Dim randRow As Long, randCol As Long
  
  If HasFood Then Exit Sub
  
  Randomize
  randRow = WorksheetFunction.RandBetween(Board.Row, Board.Row + (Board.Rows.Count - 1))
  randCol = WorksheetFunction.RandBetween(Board.Column, Board.Column + (Board.Columns.Count - 1))
  
  Set FoodLocation = slakeSheet.Cells(randRow, randCol)
  If IsEmpty(FoodLocation) Then
    FoodLocation = 0
    HasFood = True
  End If
End Sub

Private Sub UpdateScore()
  Dim speedPercentage As Double
  Score = Score + 100
  
  If GameSpeed > MaxSpeed Then
    GameSpeed = GameSpeed - Accelerate
    speedPercentage = ((StartSpeed - GameSpeed) / Accelerate) / ((StartSpeed - MaxSpeed) / Accelerate)
    Speed.TextFrame2.TextRange.Text = Format(speedPercentage, "##%")
    Gauge.Width = GaugeMax * speedPercentage
  ElseIf Level = 10 Then
    GameSpeed = GameSpeed - Overdrive
    Speed.TextFrame2.TextRange.Text = "OVERDRIVE"
  Else
    Speed.TextFrame2.TextRange.Text = "MAX"
  End If

  If Score Mod 2000 = 0 And Level < 11 Then LoadNextLevel
End Sub

Private Sub LoadNextLevel()
  Dim LevelRange As Range
  
  Board.ClearContents
  HasFood = False
  
  Level = Level + 1
  Set LevelRange = slakeSheet.Range("LEVEL" & Level)
  LevelRange = 3
  
  PlaceSnake
  MoveBuffer = ""
  DisplayMessage "LEVEL " & Level, True, 3
End Sub

Private Sub DisplayMessage(Message As String, TimeMessage As Boolean, Optional TimerCount As Long = 3)
  Dim screenTint As Shape, screenMessage As Shape, screenTimer As Shape, t As Single
  
  Set screenTint = slakeSheet.Shapes("ScreenTint")
  Set screenMessage = slakeSheet.Shapes("ScreenMessage")
  Set screenTimer = slakeSheet.Shapes("ScreenTimer")
  
  If Message = "" Then
    screenTint.Visible = False
    screenMessage.Visible = False
    screenTimer.Visible = False
  Else
    screenTint.Visible = True
    screenMessage.TextFrame2.TextRange.Text = Message
    screenMessage.Visible = True
    If TimeMessage Then
      screenTimer.TextFrame2.TextRange.Text = "GET READY"
      screenTimer.Visible = True
      While TimerCount > -1
        t = Timer + 1
        While Timer <= t
          DoEvents
        Wend
        screenTimer.TextFrame2.TextRange.Text = TimerCount
        TimerCount = TimerCount - 1
        DoEvents
      Wend
      DisplayMessage "", False
    End If
  End If
End Sub

Private Sub StopGame()
  GameOver = True
End Sub
